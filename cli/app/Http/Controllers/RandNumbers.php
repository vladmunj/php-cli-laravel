<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Helpers\RandNumbers\Initializator;
use App\Helpers\RandNumbers\Executor;

class RandNumbers extends BaseController
{
    public function __construct(){

    }

    public static function init(){
    	$initC = new Initializator();
        $execC = new Executor();

    	echo $initC->generateData();

        $execC->sort($initC->loadData());

        echo $initC->showOutput('num');
    }
}
