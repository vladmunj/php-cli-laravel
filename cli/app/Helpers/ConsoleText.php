<?php namespace App\Helpers;

use NunoMaduro\Collision\ConsoleColor;

class ConsoleText{
	public static function raw($color,$text){
		$color = new ConsoleColor;
		echo($color->apply($color,$text."\n"));
	}
}

?>