<?php namespace App\Helpers\OpenNet;

use Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;

class HttpClient{
	private $page;

	public function __construct(){
		$page = Http::get(env('OPENNET_URL'));
		$this->page = $page->body();
	}

	public function crawler(){
		$crawler = new Crawler();
		$crawler->addHtmlContent($this->page);

		$elements = $crawler->filter('.ttxt')->eq(0)->children()->filter('tr');

		$maxItems = env('OPENNET_MAX_ITEMS');
		$out = [];
		$j = 0;
		foreach($elements as $i => $tr){
			if($j == $maxItems) break;

			$el = new Crawler($tr);
			$linkEl = $el->filter('.tnews > a');
			if($linkEl->count() == 0) continue;
			$out[$j]['title'] = $linkEl->text();
			$out[$j]['href'] = $linkEl->attr('href');

			$dateEl = $el->filter('.tdate');
			if($dateEl->count() == 0) continue;
			$out[$j]['date'] = $dateEl->text();

			$j++;
		}

		return $out;
	}

	private function checkval($arr,$key){
		if(!isset($arr[$key]) || empty($arr[$key])) return false;
		return true;
	}

	public function output($data){
		foreach($data as $id => $item){
			$row = [];
			$row[] = '#'.(++$id);
			if($this->checkval($item,'title')) $row[] = 'заголовок: '.$item['title'];
			if($this->checkval($item,'link')) $row[] = 'ссылка: '.$item['link'];
			if($this->checkval($item,'date')) $row[] = 'дата: '.$item['date'];
			dump('['.join(",",$row).']');
		}
	}
}

?>