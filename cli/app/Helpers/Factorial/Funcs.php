<?php namespace App\Helpers\Factorial;

class Funcs{
	public static function fact1($n){
		$fact = 1;
		for($i=1;$i<=$n;$i++){
			$fact *= $i;
		}

		return $fact;
	}

	public static function fact2($n){
		return array_product(range(1,$n));
	}
}

?>