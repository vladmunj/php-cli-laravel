<?php namespace App\Helpers\RandNumbers;

use App\Helpers\RandNumbers\Executor;
use App\Models\RandNumber;
use Illuminate\Console\Command;

class Initializator extends Command{
	public function generateData($execC){
		$max = env('RAND_NUMBERS_MAX');
		for($i=1;$i<=$max;$i++){
			echo $execC->setNum(rand(1,100));
		}
	}

	public function loadData(){
		return RandNumber::all();
	}

	public function showOutput($key){
		$data = $this->loadData();
		foreach($data as $item){
			dump($item->$key);
		}
	}
}

?>