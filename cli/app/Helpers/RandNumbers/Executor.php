<?php namespace App\Helpers\RandNumbers;

use App\Models\RandNumber;
use Illuminate\Support\Facades\Redis;
use App\Helpers\ConsoleText;

class Executor{
	private $sorted;

	public function truncate(){
		RandNumber::truncate();
	}

	public function setNum($num){
		$model = new RandNumber;
		$model->num = $num;
		$model->save();

		return $num."\n";
	}

	private function toArray($array,$key){
		$res = [];
		foreach($array as $item){
			$res[] = $item->$key;
		}

		return $res;
	}

	public function sort($data){
		$arr = $this->toArray($data,'num');
		usort($arr,function($a,$b){
			if($a == $b) return 0;
			return ($a < $b) ? -1 : 1;
		});

		return $arr;
	}

	public function updateData($arr){
		foreach($arr as $num){
			$this->setNum($num);
		}
	}
}

?>