<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models;
use Illuminate\Support\Facades\Redis;

class RandNumbersCreate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $randnumber;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($randnumber)
    {
        $this->randnumber = $randnumber;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Redis::set('num',$this->randnumber);
        // $model = new Models\RandNumber;
        // $model->num = $this->randnumber;
        // $model->save();
    }

    public function failed(){
        echo 'Ошибка при выполнении';
    }

    public function getResponse(){
        return $this->randnumber;
    }
}
