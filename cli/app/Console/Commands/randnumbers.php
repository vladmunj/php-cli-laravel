<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\RandNumbers\Initializator;
use App\Helpers\RandNumbers\Executor;

class randnumbers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'randnumbers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Задача #1';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Комментарии работы инициатора выделены зеленым');
        $this->comment('Комментарии работы исполнителя выделены желтым');

        $this->line('');
        $this->line('');

        $initC = new Initializator();
        $execC = new Executor();

        $this->comment('Удаление старых данных ...');
        $execC->truncate();

        $this->comment('Генерация начальных данных ...');
        $initC->generateData($execC);

        $this->info('Загрузка начальных данных ...');
        $baseData = $initC->loadData();

        $this->comment('Сортировка ... ');
        $sorted = $execC->sort($baseData);

        $this->comment('Удаление первоначальных данных ...');
        $execC->truncate();

        $this->comment('Загрузка новых данных ...');
        $execC->updateData($sorted);

        $this->info('Новые данные:');
        $initC->showOutput('num');
    }
}
