<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Opennet\HttpClient;

class opennet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'opennet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Парсер новостей сайта https://www.opennet.ru/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Загрузка страницы ...');
        $http = new HttpClient();

        $this->info('Обработка html - разметки ...');
        $data = $http->crawler();

        $this->info('Результат:');
        $http->output($data);
    }
}
