<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Factorial\Funcs;

class fact1 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fact:1';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $n = env('FACT_N_VAL');
        $this->info('Значение N = '.$n);

        $fact = Funcs::fact1($n);
        $this->info('Факториал числа '.$n.' = '.$fact);
    }
}
