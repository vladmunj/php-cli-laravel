<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class userhelp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:help';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Вывод списка кастомных функций';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Список команд');
        $this->info('php artisan randnumbers - задача #1 (сортировка чисел)');
        $this->info('php artisan opennet - задача #2 (парсер новостей сайта https://www.opennet.ru/)');
        $this->info('php artisan fact:1 - задача #3 (вычисление факториала, 1 способ)');
        $this->info('php artisan fact:2 - задача #3 (вычисление факториала, 2 способ)');
    }
}
